/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author bluvison
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    public TextField firstIn;
    public TextField secondIn;
    public Button btnSum;
    public Button btnSub;
    public Button btnMult;
    public Button btnDiv;
    public Label getResult;

    @FXML
    private void actSum(ActionEvent event) {

        double first = Double.parseDouble(firstIn.getText());
        double second = Double.parseDouble(secondIn.getText());
        getResult.setText(String.valueOf(first + second));

    }

    @FXML
    private void actSub(ActionEvent event) {

        double first = Double.parseDouble(firstIn.getText());
        double second = Double.parseDouble(secondIn.getText());
        getResult.setText(String.valueOf(first - second));
    }

    @FXML
    private void actMult(ActionEvent event) {

        double first = Double.parseDouble(firstIn.getText());
        double second = Double.parseDouble(secondIn.getText());
        getResult.setText(String.valueOf(first * second));
    }

    @FXML
    private void actDiv(ActionEvent event) {

        double first = Double.parseDouble(firstIn.getText());
        double second = Double.parseDouble(secondIn.getText());
        getResult.setText(String.valueOf(first / second));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }

}
